package com.etc.inheritance

/**
 * scala com.etc.inheritance.MusicDelegation
 * 
 * C
 * B
 * A
 * Base1 
 */

trait Base1 {
    def print() { println("Base1") }
}

trait A extends Base1 {
    override def print() { println("A"); super.print() }
}

trait B extends Base1 {
    override def print() { println("B"); super.print() }
}

class Base2 {
    def print() { println("Base2") }
}

class C extends Base2 with A with B { // first goes to B, then A, then Base2 
    override def print() { println("C"); super.print() }
}

object MusicDelegation {
    def main(args : Array[String]) = {
      (new C).print()
    }
}