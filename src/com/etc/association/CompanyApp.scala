package com.etc.association

/**
 * strong aggs => composition
 * company (must) has departments and vice versa (filled diamond)
 * Spotify
   --- Engineering
   --- Recruiting
 * @author prayagupd
 * @date 03-20-2015, usa
 * http://www.scala-lang.org/old/node/125
 */

class Company (name_ : String, departments_ : List[Department] ) {
  val name : String = name_
  val departments : List[Department] = departments_
}


class Department (name_ : String) {
  val name : String = name_
}

class Position (name_ : String, salary_ : Double ) {
  val name : String = name_
  val salary : Double = salary_
}

class Employee (name_ : String) {
  val name : String = name_
}

object CompanyApp {
  def main(args : Array[String]) = {
    val engineering = new Department("Engineering")
    val recruting = new Department("Recruiting")
    val list = List(engineering, recruting)
    
    val company = new Company("Spotify", list)
    
    println("" + company.name)
    
    for(department <- company.departments) {
      println ("   --- " + department.name)
    }
  }
}