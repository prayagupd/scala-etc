/**
 * Created by 984493
 * on 5/7/2015.
 */

object BitTest {

  def update(N : Int, M : Int, from : Int, to : Int) : Int = {
    val max = ~0
    val left = max - ((1 << to) - 1)
    val right = ((1 << from) - 1)

    val mask = left | right

    return (N & mask) | (M << from)
  }

  def main(args : Array[String]) = {
    val N = 10000000000
    val M = 10101
    val from = 2
    val to = 6
    println("after update " + update(N, M, from, to))
  }
}
