package com.etc.fp

/**
 * https://coderwall.com/p/4l73-a/scala-fold-foldleft-and-foldright 
 * http://oldfashionedsoftware.com/2009/07/30/lots-and-lots-of-foldleft-examples/
 * 
 */

class Employee (val name: String, 
                 val age: Int, 
                 val sex: Symbol)

object Employee {
  def apply(name: String, age: Int, sex: Symbol) = 
    new Employee(name, age, sex)
}

object FoldTests {
  
  def count(list: List[Any]): Int =
    list.foldLeft(0)((sum,_) => 
      sum + 1)
  
  def main(args : Array[String] ) = {
    
    //1
    val numbers = List(5, 4, 8, 6, 2);
    //initialSumState=0
    val sum = numbers.fold(1) { (initialSumState, y) => 
      initialSumState + y 
    }
    println(s"sum = ${sum}") 
  
    //2
    val fooList = Employee("Hugh Jass", 25, 'male) ::
                  Employee("Biggus Dickus", 43, 'male) ::
                  Employee("Incontinentia Buttocks", 37, 'female) ::
                  Nil
              
  /**
   * The primary difference is the order in which the fold operation iterates through the collection in question. 
   * foldLeft  => starts on the left side—the first item—and iterates to the right; 
   * foldRight => starts on the right side—the last item—and iterates to the left. 
   * fold      => goes in no particular order.              
   */
  val stringList = fooList.foldLeft(List[String]()) { (z, f) =>
    val title = f.sex match {
      case 'male => "Mr."
      case 'female => "Ms."
    }
    z :+ s"$title ${f.name}, ${f.age}"
  }
  
  println(""+stringList(0))
  // stringList(0)
  // Mr. Hugh Jass, 25
  
  println(""+stringList(1))
  // stringList(2)
  // Ms. Incontinentia Buttocks, 37
  }
}

