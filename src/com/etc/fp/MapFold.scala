package com.etc.fp

/**
 * Given a list of facebook users, and their activities
 * Find if each facebook user has more comments than likes, 
 *
 * eg. data
 * ["A" -> ("like", "comment", "like", "comment", "like") ]

 */

object MapFold {
  
  def map (x : String ): Int = {
    if (x=="like") 
       1 
    else 
      -1;
  }
  
  def fold(x : Int, y : Int): Int = {
    x + y;
  }
  
  def analyseData (userName : String, data : Array[String]) = {
    //1. map
    val mapResultArray  = new Array[Int](data.length)    
    data.zipWithIndex.foreach { case (elem, i) => 
      mapResultArray(i)= map(elem)
      println(s" $i (${mapResultArray(i)}) => ${map(elem)}")
    }
    
    //2. fold
    var initialState = 0;
    val foldResultArray = new Array[Int](data.length)
    data.zipWithIndex.foreach { case (element, i) =>
      initialState = fold(initialState, mapResultArray(i));
      foldResultArray(i) = initialState
    }
    
    //3. interpretation
    if (initialState > 0) {
      println(s"User ${userName} has more likes.")
    } else if( initialState < 0) {
      println(s"User ${userName} has more comments.")
    } else {
      println(s"User ${userName} has equal comments and likes.")
    }
  }
  
  def main (args : Array[String]) = {
    //val data = Array ("like", "comment", "like", "comment", "like")
    val userData = Map("prayag upd"    -> Array ("like", "comment", "like", "comment", "like"), 
                       "steven wilson" -> Array ("like", "comment", "like", "comment", "comment"))
                       
    userData.foreach({ case (userName, data) =>
      analyseData(userName, data)
    })                      
  }
  
}