package com.etc.patterns.strategy

/**
  * scalac OperationStrategy.scala
  * http://alvinalexander.com/scala/how-scala-killed-oop-strategy-design-pattern
  */

object OperationStrategy extends App {
 
  def add(a: Int, b: Int) = a + b
  def subtract(a: Int, b: Int) = a - b
  def multiply(a: Int, b: Int) = a * b
   
  def execute(callback:(Int, Int) => Int, x: Int, y: Int) = 
		callback(x, y)
 
   //def main(args : Array[String]) = {
      println("Add:      " + execute(add, 3, 4))
      println("Subtract: " + execute(subtract, 3, 4))
      println("Multiply: " + execute(multiply, 3, 4))     
   //}

}
