package com.etc.patterns.observer

trait Observer[S] {
    def receiveUpdate(subject: S);
}