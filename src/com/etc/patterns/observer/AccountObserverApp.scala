package com.etc.patterns.observer

class Account(initialBalance: Double) {
    private var currentBalance = initialBalance
    def balance = currentBalance
    def deposit(amount: Double)  = currentBalance += amount
    def withdraw(amount: Double) = currentBalance -= amount
}

class ObservedAccount(initialBalance: Double) extends Account(initialBalance) with SubjectObservable[Account] {
    override def deposit(amount: Double) = {
        super.deposit(amount)
        notifyObservers()
    }
    
    override def withdraw(amount: Double) = {
        super.withdraw(amount)
        notifyObservers()
    }
}


class AccountReporter extends Observer[Account] {
    def receiveUpdate(account: Account) =
        println("Observed balance change: "+account.balance)
}

object AccountObserverApp {
  def main(args : Array[String]) = {
    val oa = new ObservedAccount(100.0)
    val ar = new AccountReporter
    
    oa.addObserver(ar)
    oa.deposit(40.0)
    oa.withdraw(40.0)
  }
}